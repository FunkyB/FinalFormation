<?php
/**
 * Created by PhpStorm.
 * User: wes
 * Date: 29/05/18
 * Time: 14:10
 */

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;


class Mail
{
    /**
     * @Assert\NotBlank(message="pas de vide")
     */
    protected $sender;

    /**
     * @Assert\NotBlank(message="pas de vide")
     * @Assert\Regex(
     *     pattern="/[a-zA-Z]+/",
     *     match=true,
     *     message="pas de chiffre"
     * )
     */
    protected $dest;

    /**
     * @Assert\NotBlank(message="pas de vide")
     * @Assert\Regex(
     *     pattern="/[a-zA-Z]+/",
     *     match=true,
     *     message="pas de chiffre"
     * )
     */
    protected $object;

    /**
     * @Assert\NotBlank(message="pas de vide")
     */
    protected $body;

    /**
     * @param mixed $sender
     * @return Mail
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     * @return Mail
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDest()
    {
        return $this->dest;
    }

    /**
     * @param mixed $dest
     * @return Mail
     */
    public function setDest($dest)
    {
        $this->dest = $dest;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     * @return Mail
     */
    public function setObject($object)
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return Mail
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }


}