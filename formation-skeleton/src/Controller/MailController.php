<?php

namespace App\Controller;

use App\Entity\Mail;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class MailController extends Controller
{

    /**
     * @Route(
     * "new_mail",
     *     name="new_mail"
     *     )
     */
    public function new_mail(Request $request)
    {
        $mail = new Mail();
        $mail->setBody('');
        $mail->setDest('b.feron@yopmail.com');
        $mail->setObject('test');
        $mail->setSender('');

        $form = $this->createFormBuilder($mail)
            ->add('sender', TextType::class)
            ->add('dest', TextType::class)
            ->add('object', TextType::class)
            ->add('body', TextareaType::class)
            ->add('save', SubmitType::class, array('label'=>'Send'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

        }else{

        }

        return $this->render('mail/new.html.twig', ['form'=>$form->createView()]);
    }
}