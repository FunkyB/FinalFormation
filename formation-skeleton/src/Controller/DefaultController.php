<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller{

    public function index()
    {
        echo 'Index';
        return new Response();
    }

    public function anotherRoute()
    {
        echo 'Another Route !';
        return new Response();
    }

    /**
     * @Route(
     *     "hello/{name}",
     *     name="hello",
     *     requirements={"name"="[a-zA-Z]+"},
     *     defaults={"name"="bryan"}
     *     )
     * @Method({"GET"})
     *
     */
    public function hello($name)
    {
        $slugifier = new \App\Service\Slugifier;

        if ($name =='test'){
            return $this->forward('\App\Controller\DefaultController::hello', ['name'=>'mouahahah']);
        }
        if ($name == 'alex')
        {
            return $this->redirectToRoute('hello',['name'=>'mouba'], 301);
        }
        if ($name == 'ours')
        {
            throw $this->createNotFoundException('VOUS NE PASSEREZ PAS !!!');
        }

        echo 'Hello '.$slugifier->slugify($name);

        return new Response();
    }
}
