<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class createUserCommand extends Command
{

    public function configure()
    {
        $this->setName('app:test');
        $this->setDescription('commande test');
        $this->setHelp('no help');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
       $output->writeln('hello');

       $kernel = $this->getApplication()->getKernel();

       $mailer = $kernel->getContainer()->get('mailer');

       $message = (new \Swift_Message('hello'))
                ->setFrom('b.feron@webetsolutions.com')
                ->setTo('b.feron@yopmail.com')
                ->setBody('tttt');

       $mailer->send($message);


    }


}