<?php

namespace App\Controller;

use App\Entity\Movie;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MovieController extends Controller
{
    /**
     * @Route("/movie", name="movie")
     */
    public function index()
    {

    }

    /**
     * @Route("add", name="add")
     */
    public function add(EntityManagerInterface $entityManager)
    {
        $movie = new Movie();
        $movie->setName('test')->setVisa('123456')->setYear('1970');

        $entityManager->persist($movie);
        $entityManager->flush();

        return new Response('Nouveau film crée ' . $movie->getId());
    }

    /**
     * @Route("movie/{id}", name="movie")
     */
    public function movie(Movie $movie)
    {
        return $this->render('movie/movie.html.twig', ['movie'=>$movie]);

    }


    public function moviesExceptId($id, EntityManagerInterface $entityManager)
    {
        $movies = $entityManager->getRepository(Movie::class)->findAllExceptId($id);
        return $this->render('movie/movies_except.html.twig', ['movies'=>$movies]);
    }

    /**
     * @Route("delMovie/{id}", name="DelMovie")
     */
    public function DelMovie($id, Movie $movie, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($movie);
        $entityManager->flush();

        return new Response('Film ' . $id.' : '.$movie->getName());

    }

    /**
     * @Route("movies", name="movies")
     */
    public function movies(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $movies = $entityManager->getRepository(Movie::class)->findAll();

        $logger->info('Bryan was here in movies!');

        return $this->render('movie/movies.html.twig', ['movies'=>$movies]);

    }
}
